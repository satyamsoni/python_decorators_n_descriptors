## Python Training
---

### Topics Covered:
- Decorators
  - [Functional](decorators/functional/__init__.py)
    - [single level](decorators/functional/single_level_decorator/decorator.py)
    - [multi level](decorators/functional/multi_level_decorator/decorator.py)
  - [Class Based](decorators/class_based_decorators/decorators.py)
  - Decorator chaining
  - [Dependancy injection using decorators](decorators/dependancy_injection/utils/decorators.py#inject_decorator)

#### About

This project contains code fragments covered in the training on `Decorators`.

`Decorators` are built on the top on `closures` and are used to extend the functionality of existing callable. `Decorators` may also be considered as `Proxy` in cases to prevent direct reference to underlying object.

It also is in accordance with`O` in `SOLID` principles which states **Any object should be open for extension but closed for modifications** .

#### Project Setup

- Installing dependencies

    ```bash
    python3 -m pip install poetry
    poetry install
    ```

- Forking new shell

    ```bash
    poetry shell
    ```

#### Running Code

```bash
    python file.py
```

![Decorators Flow Daigram](decorators.jpg)

Happy Coding :)

`Follow me:` [![Twitter](https://img.shields.io/badge/Twitter-%231DA1F2.svg?style=for-the-badge&logo=Twitter&logoColor=white)](https://twitter.com/satyam_soni1306) [![LinkedIn Follow](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/-satyamsoni/) [![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/satyamsoni2211)