# Class based decorators
# Advanced level concepts

# const a = {name:"satyam"}
# a.prototype.call = function(n) {console.log(n)} # method callable
# a.call("abc")
# VM182:1 Uncaught TypeError: a.call is not a function
#    at <anonymous>:1:3


# class Decorator{
#     function constructor(props) {
#         this.func_ = props.func
#     }
# }
from typing import Any
from functools import wraps


class Decorator(object):

    def __init__(self, func: callable) -> None:
        self.func_ = func
        self.__doc__ = func.__doc__
        self.__class__.__call__ = wraps(func)(self.__class__.__call__)

    def __call__(self, *args, **kwargs) -> Any:  # magic methods
        return self.func_(*args, **kwargs)


# a.call("abc")
# a = Decorator(func)
# a()

# @Decorator


@Decorator
def foo_bar():
    "This is from foo bar"
    print("from foo bar")


if __name__ == "__main__":
    # f = Decorator(foo_bar)
    # f()
    foo_bar()
