from flask import Flask, jsonify
from base_app import base_bp
from utils.exceptions import InvalidValueException

app = Flask("fake_app")

app.register_blueprint(base_bp)


@app.errorhandler(InvalidValueException)
def handleException(e: InvalidValueException):
    res = jsonify(**e.message)
    res.status_code = e.status_code
    return res


if __name__ == "__main__":
    app.run()
