from flask import Blueprint, jsonify
from utils.casters import Query
from utils.decorators import inject_decorator

base_bp = Blueprint("base_bp", import_name="base_bp")


@base_bp.route("/ping", methods=["GET"])
def health_check():
    return jsonify(status="healthy")


@base_bp.route("/home", methods=["GET"])
@inject_decorator
def home(username: Query(name="username"), age: Query(name="age", default=23)):
    return jsonify({"name": username, "age": age})
