from flask import request
from typing import Any
from .exceptions import InvalidValueException


class Caster(object):
    type_: str = "args"
    message_default_: str = "Query Parameters"

    def __init__(self, default=..., name=None) -> None:
        self._name = name
        self.default = default

    def __call__(self) -> Any:
        val = self.cast()
        if not val and self.default == Ellipsis:
            raise InvalidValueException(message={
                "error": f"could not find {self._name} in {self.message_default_}."
            })
        return val or self.default

    def cast(self):
        return getattr(request, self.type_).get(self._name, None)
