from .base import Caster


class Path(Caster):
    type_: str = "view_args"
    message_default_: str = "Path parameters"


class Query(Caster):
    pass


class Body(Caster):
    type_: str = "json"
    message_default_: str = "Request Body"
