import inspect
from .base import Caster


def inject_decorator(func):
    f = inspect.getfullargspec(func)
    annotations_ = {i: j for i, j in f.annotations.items()
                    if isinstance(j, Caster)}

    def __inner__(*args, **kwargs):
        nkwargs = {i: j() for i, j in annotations_.items()}
        return func(*args, **kwargs, **nkwargs)
    return __inner__
