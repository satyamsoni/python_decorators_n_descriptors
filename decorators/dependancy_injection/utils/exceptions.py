class InvalidValueException(Exception):
    def __init__(self, message, *args: object, status_code=400) -> None:
        self.message = message
        self.status_code = status_code
        super().__init__(*args)
