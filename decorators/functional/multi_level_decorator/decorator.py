# multi level decorator
#
import time


def log_it(timezone="asia/kolkata"):  # root level
    """
    Parent function to accept arguments from user
    Args:
        timezone (str, optional): timezone. Defaults to "asia/kolkata".
    """
    def decorator(func):  # sub level
        def logger(*args, **kwargs):  # inner / proxy wrapper
            t = time.time()
            res = func(*args, **kwargs)  # function call
            print(f"{timezone} code executed in  {time.time()-t} secs")
            return res
        return logger  # callable
    return decorator  # callable


def fake():
    print("from fake")


@log_it()
def foobar():
    time.sleep(1)


@log_it()
def foo_bar():
    time.sleep(0.5)


if __name__ == "__main__":
    f = log_it("foobar")  # callable
    s = f(fake)  # callable
    print(f.__name__, s.__name__)
    s()  # proxy
    foo_bar()
