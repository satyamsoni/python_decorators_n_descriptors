from functools import wraps

# Proxy design pattern


def security(func):  # callable
    @wraps(func)
    def rule(*args, **kwargs):
        """
        This is the proxy function
        """
        # list with allowed members
        allowed_members = ["family", "owner", "owners"]
        filtered_args = []  # empty list, array
        for i in args:  # for loop in python
            if i.lower() in allowed_members:  # conditional expression
                filtered_args.append(i)
        return func(*filtered_args, **kwargs)
    return rule

# Problem statement
# house only owners are allowed
# and servents are prohibited
# add an additional layer on your door
# allow owners or family members
# stop servents


@security
def house(*args):
    """
        This is a house function
    """
    print(args)


@security
def house_with_subpartitions(*args, **kwargs):
    """
    This is a function with kwargs
    """
    print(f"people allowed are {args=}")
    print(f"partitions are {kwargs=}")


if __name__ == "__main__":
    house("family", "owner", "servent", "owner", "servent")
    house_with_subpartitions("family", "owner", "servent",
                             "owner", "servent",
                             part1="kitchen", part2="living")
