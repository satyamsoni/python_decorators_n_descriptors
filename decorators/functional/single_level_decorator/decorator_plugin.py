plugins = {}


def register_plugin(func):
    plugins[func.__name__] = func
    return func


def get_plugin(name: str):
    return plugins.get(name)


@register_plugin
def html_processor(html: str) -> str:
    return html


print(plugins)
